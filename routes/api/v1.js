var express = require('express');
var router = express.Router();
pageSize = 20;
var knownKeys = ["page",
                 "_id",
                 "city",
                 "state",
                 "name",
                 "address",
                 "tel",
                 "email",
                 "text"
                 ];

function printDate()
{
  var d = new Date();
  var curr_date = d.getDate();
  var curr_month = d.getMonth();
  var curr_year = d.getFullYear();
  console.log( curr_date + "-" + curr_month + "-" + curr_year);
}

/* GET default */
router.get('/', function(req, res) {
  res.send('version 1.0 API');
});

/* GET college listing. */
router.get('/colleges', function(req, res) {
  var db = req.db;
  printDate();
  console.log(req.query);
  var start_id = req.query.start_id || 0; 
  var page = req.query.page || 0;
  var queryParams = req.query;
  delete queryParams.start_id;
  delete queryParams.page;
  var query = {};

  //Check if text search first
  if(!(typeof queryParams["text"] === "undefined") && queryParams["text"] != null )
  {
        query = {$text: {$search :  queryParams["text"] } };
        db.collection('collegecollection').find(query,{score:{$meta:"textScore"}}).sort({score:{$meta:"textScore"}}).skip(page*pageSize).limit(pageSize).toArray(function(err,items){
                  res.jsonp(items);
        });
  }
  else
  {
          for(var key in queryParams)
                if(knownKeys.indexOf(key) != -1)
        	        query[key] = new RegExp(queryParams[key], "i");
          //console.log(query);
          //console.log("page : "+ page + " start_id :"+ start_id);
          // TODO :  work on a better pagination strategy [ range based pagination ?]
          db.collection('collegecollection').find(query).skip(page*pageSize).limit(pageSize).toArray(function(err,items){
                  res.jsonp(items);
          });
  }
});

module.exports = router;
